//===- ObservationExpansion.cpp - Observation Pseudo-instruction Expansion ===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements a MachineFunctionPass that expands observation-related
// pseudo-instructions.
//
//===----------------------------------------------------------------------===//

#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/InitializePasses.h"

using namespace llvm;

namespace {
struct ObservationExpansion : public MachineFunctionPass {
  static char ID; // Pass identification, replacement for typeid
  ObservationExpansion() : MachineFunctionPass(ID) {
    initializeObservationExpansionPass(*PassRegistry::getPassRegistry());
  }

  StringRef getPassName() const override {
    return "Observation Pseudo-instruction Expansion";
  }
  bool runOnMachineFunction(MachineFunction &F) override;
};
}

bool ObservationExpansion::runOnMachineFunction(MachineFunction &MF) {
  bool Changed = false;
  for (auto &MBB : MF) {
    DenseMap<Register, const MachineInstr *> OperandToOpaqueReadMap;
    SmallVector<MachineInstr *, 8> ToBeRemoved;
    for (auto &MI : MBB) {
      // FIXME: this relies on the assumption that OPAQUE_READ is in the same
      // MBB as OPACIY and is only used once.
      if (MI.getOpcode() == TargetOpcode::OPAQUE_READ) {
        OperandToOpaqueReadMap[MI.getOperand(0).getReg()] = &MI;
      } else if (MI.isObserve()) {
        MachineInstrBuilder MIB = BuildMI(MBB, MI, MI.getDebugLoc(), MI.getDesc());
        auto *NewMI = MIB.getInstr();
        NewMI->addOperand(MI.getOperand(0));
        NewMI->addOperand(MI.getOperand(1));
        NewMI->addOperand(MI.getOperand(2));
        for (unsigned i = 3; i < MI.getNumOperands(); ++i) {
          MachineOperand &MO = MI.getOperand(i);
          if (!MO.isReg()) {
            NewMI->addOperand(MO);
          } else if (OperandToOpaqueReadMap.lookup(MO.getReg())) {
              const auto *OR = OperandToOpaqueReadMap[MO.getReg()];
              NewMI->addOperand(OR->getOperand(1));
              NewMI->addOperand(OR->getOperand(2));
              i++;
              OperandToOpaqueReadMap.erase(MO.getReg());
          } else {
            NewMI->addOperand(MO);
          }
        }
        ToBeRemoved.push_back(&MI);
        Changed = true;
      }
    }
    for (auto *MI : ToBeRemoved)
      MBB.erase(MI);
  }

  return Changed;
}

char ObservationExpansion::ID = 0;
char &llvm::ObservationExpansionID = ObservationExpansion::ID;
INITIALIZE_PASS(ObservationExpansion, "observation-expansion",
                "Observation Pseudo-instruction Expansion", false, false)
